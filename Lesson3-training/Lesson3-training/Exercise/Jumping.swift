//
//  Jumping.swift
//  Lesson3-training
//
//  Created by Timofey Privalov on 21.09.2021.
//

import Foundation

class Jumping: Exercise {
    var name: String = "Прыжки"

    var type: String = "ноги"

    func start() {
        print("начинаем прыжки")
    }

    func stop() {
        print("заканчиваем прыжки")
    }


}
