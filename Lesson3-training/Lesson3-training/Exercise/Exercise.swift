//
//  Exercise.swift
//  Lesson3-training
//
//  Created by Timofey Privalov on 21.09.2021.
//

import Foundation

protocol Exercise {
    var name: String { get }
    var type: String { get }

    func start()
    func stop()
}
