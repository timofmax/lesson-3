//
//  SwingPress.swift
//  Lesson3-training
//
//  Created by Timofey Privalov on 21.09.2021.
//

import Foundation

class SwingPress: Exercise {
    var name: String = "Жим Штанги"

    var type: String = "Упражнение для ног"

    func start() {
        print("начнем упражнение для ног")
    }

    func stop() {
        print("закончим упражнение для ног")
    }


}
