//
//  FactoryExercises.swift
//  Lesson3-training
//
//  Created by Timofey Privalov on 21.09.2021.
//

import Foundation

enum Exercises {
    case jumping, squarts, swingPress
}

class FactoryExercises {
    static let defaultFactory = FactoryExercises()
    
    func createExercise(name: Exercises) -> Exercise {
        switch name {
        case .jumping: return Jumping()
        case .squarts: return Squarts()
        case .swingPress: return SwingPress()
        default:
            break
        }
    }
}
