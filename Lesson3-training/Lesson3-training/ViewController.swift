//
//  ViewController.swift
//  Lesson3-training
//
//  Created by Timofey Privalov on 21.09.2021.
//

import UIKit

class ViewController: UIViewController {
    var exerciseArray = [Exercise]()
    override func viewDidLoad() {
        super.viewDidLoad()
        createExercise(exname: .jumping)
        createExercise(exname: .squarts)
        createExercise(exname: .swingPress)
        runExercise()
    }

    func createExercise(exname: Exercises) {
        let newExercise = FactoryExercises.defaultFactory.createExercise(name: exname)
        exerciseArray.append(newExercise)
    }

    func runExercise() {
        for ex in exerciseArray {
            ex.start()
            ex.stop()
        }
    }

}

