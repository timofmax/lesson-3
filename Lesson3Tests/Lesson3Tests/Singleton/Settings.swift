//
//  Settings.swift
//  Lesson3Tests
//
//  Created by Timofey Privalov on 21.09.2021.
//

import Foundation
import UIKit

class Settings {
    //Mагия, которая нигде не объясняется:
    //Возвращает ссылку, на созданный объект
    static let shared = Settings()
    var colorStyle = UIColor.white
    var volumeLevel: Float = 1.0
    private init() {}
    //Пока приложение живо - синглтон находится в памяти
    // Apple юзает - UIApplication.shared.applicationBadgeNumber = 1
}
