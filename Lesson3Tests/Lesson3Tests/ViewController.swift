//
//  ViewController.swift
//  Lesson3Tests
//
//  Created by Timofey Privalov on 20.09.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        Settings.shared.volumeLevel = 1.0
        print(Settings.shared.volumeLevel)
        Settings.shared.volumeLevel = 2.0
        print(Settings.shared.volumeLevel)
        UIApplication.shared.applicationIconBadgeNumber = 2
    }
}
